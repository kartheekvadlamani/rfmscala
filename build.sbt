name := "RFMSCALA"

version := "1.0"

scalaVersion := "2.11.8"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.11
libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "2.0.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-streaming_2.11
libraryDependencies += "org.apache.spark" % "spark-streaming_2.10" % "2.0.0"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.21"

// https://mvnrepository.com/artifact/com.datastax.spark/spark-cassandra-connector_2.10
libraryDependencies += "com.datastax.spark" % "spark-cassandra-connector_2.10" % "1.6.0-M1"

// https://mvnrepository.com/artifact/com.datastax.spark/spark-cassandra-connector-java_2.10
libraryDependencies += "com.datastax.spark" % "spark-cassandra-connector-java_2.10" % "1.6.0-M1"

// https://mvnrepository.com/artifact/org.apache.cassandra/cassandra-thrift
libraryDependencies += "org.apache.cassandra" % "cassandra-thrift" % "2.1.2"

// https://mvnrepository.com/artifact/org.apache.spark/spark-streaming-twitter_2.10
libraryDependencies += "org.apache.spark" % "spark-streaming-twitter_2.10" % "1.6.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-streaming-kafka_2.10
libraryDependencies += "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.6.2"

// https://mvnrepository.com/artifact/junit/junit
libraryDependencies += "junit" % "junit" % "4.12"

// https://mvnrepository.com/artifact/org.apache.kafka/connect-json
libraryDependencies += "org.apache.kafka" % "connect-json" % "0.10.0.1"

// https://mvnrepository.com/artifact/org.apache.avro/avro
libraryDependencies += "org.apache.avro" % "avro" % "1.8.0"

// https://mvnrepository.com/artifact/com.101tec/zkclient
libraryDependencies += "com.101tec" % "zkclient" % "0.3"

// https://mvnrepository.com/artifact/xalan/serializer
libraryDependencies += "xalan" % "serializer" % "2.7.2"


