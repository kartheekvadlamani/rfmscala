package Hadoop

import java.math.{BigDecimal, BigInteger}
import java.net.URI
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

import RFMCustomer.Customer
import RFMCustomerAddress.CustomerAddress
import RFMOrder.Order
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.api.java.function.Function
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.slf4j.Logger


/**
  * Created by kartheekvadlamani on 9/17/16.
  */
class Hadoop {


  def RemoveEscapeSequences(line: String): String = {

    // This is the escape sequence used to output the data (by default backslash escape commas in the string)
    // e.g. - sqoop --escaped-by \\ ...

    val escapeSequence: String = "\\,"
    var lineWithoutEscapeSequences: String = line

    if (line != null && !line.isEmpty && line.contains (escapeSequence)) {
      val regexEscapeSequence: String = "\\\\,"
      lineWithoutEscapeSequences = line.replaceAll (regexEscapeSequence, "")
    }
    lineWithoutEscapeSequences

  }


  def TryParseBigDecimal(decimalString: String): BigDecimal = {

    var decimal: BigDecimal = null
    val nullString: String = new String ("null")

    if (decimalString != null && !decimalString.isEmpty && decimalString != nullString) {

      decimal = new BigDecimal (decimalString)
    }
    decimal

  }


  def TryParseBigInteger(bigIntValue: String): BigInteger = {

    var bigInteger: BigInteger = null
    val nullString: String = new String ("null")

    if (bigIntValue != null && !bigIntValue.isEmpty && bigIntValue != nullString) {

      bigInteger = new BigInteger (bigIntValue)
    }

    bigInteger
  }


  def TryParseInteger(integerValue: String): Integer = {

    var integer: Integer = null
    val nullString: String = new String ("null")

    if (integerValue != null && !integerValue.isEmpty && integerValue != nullString) {

      integer = integerValue.toInt

    }

    integer
  }


  def TryParseBoolean(boolValue: String): Boolean = {
    var bool: Boolean = false
    val nullString: String = new String ("null")

    if (boolValue != null && !boolValue.isEmpty && boolValue != nullString) {
      bool = boolValue.toBoolean
    }
    bool
  }


  def TryParseDate(dateValue: String, dateFormat: SimpleDateFormat): Date = {


    var date: Date = null
    val simpleDateFormat: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)

    val nullString: String = new String ("null")

    if (dateValue != null && !dateValue.isEmpty && dateValue != nullString) date = simpleDateFormat.parse (dateValue)
    date
  }

  def deleteNewData(path: String, hdfsPath: String) {

    val conf: Configuration = getConfiguration
    val hdfs: FileSystem = FileSystem.get (URI.create (hdfsPath), conf)

    var newpath: String = null

    if (path.endsWith ("*")) {
      newpath = path substring(0, path.length () - 2)
    }

    else {
      newpath = path
    }

    if (hdfs.exists (new Path (newpath))) {
      hdfs.delete (new Path (newpath), true)
    }

  }


  def getConfiguration: Configuration = {
    val conf: Configuration = new Configuration

    conf.set ("fs.hdfs.impl", classOf [FileSystem].getName)
    conf.set ("fs.file.impl", classOf [FileSystem].getName)
    conf
  }


  def doesPathExist(path: String, hdfsPath: String, logger: Logger): Boolean = {
    val conf: Configuration = getConfiguration
    val hdfs: FileSystem = FileSystem.get (URI.create (hdfsPath), conf)
    var pathWithoutWildcard: String = null

    try {
      if (path.endsWith ("*")) {
        pathWithoutWildcard = path substring(0, path.length () - 2)
      }
      else {
        pathWithoutWildcard = path
      }

      if (hdfs.exists (new Path (pathWithoutWildcard))) true
      else false
    }
    catch {
      case ex: Exception =>
        logger.error (ex.getMessage)
        Boolean2boolean (null)
    }
  }

    def parseOrders(ordersPath: String, sc: JavaSparkContext, logger: Logger): JavaRDD[Order] = {
      sc.textFile (ordersPath).map [Order](new Function[String, Order]() {
        def call(line: String): Order = {
          try {
            val lineWithoutEscapeCharacters: String = RemoveEscapeSequences (line)
            val fields: Array[String] = lineWithoutEscapeCharacters.split (",")
            val inputDateFormat: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
            if (fields.length == 28) {
              val id: Int = fields (0).toInt
              val applicationId: Int = fields (1).toInt
              val orderId: String = if (fields (2) != null && !fields (2).isEmpty) fields (2).trim
              else null
              val orderNumber: String = if (fields (3) != null && !fields (3).isEmpty) fields (3).trim
              else null
              val status: String = if (fields (4) != null && !fields (4).isEmpty) fields (4).trim
              else null
              val financialStatus: String = if (fields (5) != null && !fields (5).isEmpty) fields (5).trim
              else null
              val customerIsGuest: Boolean = TryParseBoolean (fields (6))
              val customerId: BigInteger = TryParseBigInteger (fields (7))
              val firstName: String = if (fields (8) != null && !fields (8).isEmpty) fields (8).trim
              else null
              val lastName: String = if (fields (9) != null && !fields (9).isEmpty) fields (9).trim
              else null
              val email: String = if (fields (10) != null && !fields (10).isEmpty) fields (10).trim
              else null
              val subTotalPrice: BigDecimal = TryParseBigDecimal (fields (11))
              val totalDiscounts: BigDecimal = TryParseBigDecimal (fields (12))
              val storeCredit: BigDecimal = TryParseBigDecimal (fields (13))
              val totalPrice: BigDecimal = TryParseBigDecimal (fields (14))
              val currencyCode: String = if (fields (15) != null && !fields (15).isEmpty) fields (15).trim
              else null
              val source: String = if (fields (16) != null && !fields (16).isEmpty) fields (16).trim
              else null
              val createdAt: Date = TryParseDate (fields (17), inputDateFormat)
              val updatedAt: Date = TryParseDate (fields (18), inputDateFormat)
              val customerCreatedAt: Date = TryParseDate (fields (19), inputDateFormat)
              val isSynced: Boolean = TryParseBoolean (fields (20))
              val billingAddressId: String = if (fields (21) != null && !fields (21).isEmpty) fields (21).trim
              else null
              val shippingAddressId: String = if (fields (22) != null && !fields (22).isEmpty) fields (22).trim
              else null
              val created: Date = TryParseDate (fields (23), inputDateFormat)
              val modified: Date = TryParseDate (fields (24), inputDateFormat)
              val consumerOrderId: Integer = TryParseInteger (fields (25))
              val previousStatus: String = if (fields (26) != null && !fields (26).isEmpty) fields (26).trim
              else null
              val isPartialData: Boolean = TryParseBoolean (fields (27))
              val order: Order = new Order (id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt, isSynced, billingAddressId, shippingAddressId, created, modified, consumerOrderId, previousStatus, isPartialData)
              order
            }
            else {
              val errorMessage: String = "%s %s %s".format ("Order '%s' cannot be parsed due to invalid length of %s.", line, fields.length)
              logger.error (errorMessage)
              null
            }
          }
          catch {
            case ex: Exception =>
              logger.error ("Failed to parse order: " + line)
              null
          }
        }

      })
    }

    def parseCustomerData(customersPath: String, sc: JavaSparkContext, logger: Logger): JavaRDD[Customer] = {
      sc.textFile (customersPath).map [Customer](new Function[String, Customer]() {
        @throws[Exception]
        def call(line: String): Customer = {
          try {
            val lineWithoutEscapeCharacters: String = RemoveEscapeSequences (line)
            val fields: Array[String] = lineWithoutEscapeCharacters.split (",")
            val inputDateFormat: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
            if (fields.length == 15) {
              val id: BigInteger = TryParseBigInteger (fields (0))
              val applicationId: Int = fields (1).toInt
              val customerId: BigInteger = TryParseBigInteger (fields (2))
              val customerGroup: String = if (fields (3) != null && !fields (3).isEmpty) fields (3).trim
              else null
              val firstName: String = if (fields (4) != null && !fields (4).isEmpty) fields (4).trim
              else null
              val lastName: String = if (fields (5) != null && !fields (5).isEmpty) fields (5).trim
              else null
              val email: String = if (fields (6) != null && !fields (6).isEmpty) fields (6).trim
              else null
              val optInNewsletter: Boolean = TryParseBoolean (fields (7))
              val createdAt: Date = TryParseDate (fields (8), inputDateFormat)
              val updatedAt: Date = TryParseDate (fields (9), inputDateFormat)
              val isSynced: Boolean = TryParseBoolean (fields (10))
              val created: Date = TryParseDate (fields (11), inputDateFormat)
              val modified: Date = TryParseDate (fields (12), inputDateFormat)
              val consumerCustomerId: Integer = TryParseInteger (fields (13))
              val isPartialData: Boolean = TryParseBoolean (fields (14))
              val customer: Customer = new Customer (id, applicationId, customerId, customerGroup, firstName, lastName, email, optInNewsletter, createdAt, updatedAt, isSynced, created, modified, consumerCustomerId, isPartialData)
              customer
            }
            else {
              val errorMessage: String = "%s %s %s".format ("Customer '%s' cannot be parsed due to invalid length of %s.", lineWithoutEscapeCharacters, fields.length)
              logger.error (errorMessage)
              null
            }
          }
          catch {
            case ex: Exception =>
              logger.error ("Failed to parse customer: " + line)
              null
          }
        }
      })
    }


  def parseCustomerAddress(customerAddressPath: String, sc: JavaSparkContext, logger: Logger): JavaRDD[CustomerAddress] = {
    sc.textFile(customerAddressPath).map[CustomerAddress](new Function[String,CustomerAddress]() {
      def call(line: String): CustomerAddress = {
        try {
          val lineWithoutEscapeCharacters: String = RemoveEscapeSequences (line)
          val fields: Array[String] = lineWithoutEscapeCharacters.split (",")
          val inputDateFormat: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
          if (fields.length == 17) {
            val id: Int = TryParseInteger (fields (0))
            // val applicationId: Int = TryParseInteger(fields(1))
            val customerId: BigInteger = TryParseBigInteger (fields (1))
            val firstName: String = if (fields (2) != null && !fields (2).isEmpty) fields (2).trim
            else null
            val lastName: String = if (fields (3) != null && !fields (3).isEmpty) fields (3).trim
            else null
            val company: String = if (fields (4) != null && !fields (4).isEmpty) fields (4).trim
            else null
            val address1: String = if (fields (5) != null && !fields (5).isEmpty) fields (5).trim
            else null
            val address2: String = if (fields (6) != null && !fields (6).isEmpty) fields (5).trim
            else null
            val city: String = if (fields (7) != null && !fields (7).isEmpty) fields (7).trim
            else null
            val province: String = if (fields (8) != null && !fields (8).isEmpty) fields (8).trim
            else null
            val country: String = if (fields (9) != null && !fields (9).isEmpty) fields (9).trim
            else null
            val provinceCode: String = if (fields (10) != null && !fields (10).isEmpty) fields (10).trim
            else null
            val countryCode: String = if (fields (11) != null && !fields (11).isEmpty) fields (11).trim
            else null
            val addressType: String = if (fields (12) != null && !fields (12).isEmpty) fields (12).trim
            else null
            val zip: String = if (fields (13) != null && !fields (13).isEmpty) fields (13).trim
            else null
            val telephone: String = if (fields (14) != null && !fields (14).isEmpty) fields (14).trim
            else null
            val created: Date = TryParseDate (fields (15), inputDateFormat)
            val modified: Date = TryParseDate (fields (16), inputDateFormat)

              // need to solve the constructor problem.
            val customerAddress = new CustomerAddress (id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified)
            customerAddress
          }

          else {
            if (fields.length == 18) {
              val id: Int = TryParseInteger (fields (0))
              val applicationId: Int = TryParseInteger(fields(1))
              val customerId: BigInteger = TryParseBigInteger (fields (1))
              val firstName: String = if (fields (2) != null && !fields (2).isEmpty) fields (2).trim
              else null
              val lastName: String = if (fields (3) != null && !fields (3).isEmpty) fields (3).trim
              else null
              val company: String = if (fields (4) != null && !fields (4).isEmpty) fields (4).trim
              else null
              val address1: String = if (fields (5) != null && !fields (5).isEmpty) fields (5).trim
              else null
              val address2: String = if (fields (6) != null && !fields (6).isEmpty) fields (5).trim
              else null
              val city: String = if (fields (7) != null && !fields (7).isEmpty) fields (7).trim
              else null
              val province: String = if (fields (8) != null && !fields (8).isEmpty) fields (8).trim
              else null
              val country: String = if (fields (9) != null && !fields (9).isEmpty) fields (9).trim
              else null
              val provinceCode: String = if (fields (10) != null && !fields (10).isEmpty) fields (10).trim
              else null
              val countryCode: String = if (fields (11) != null && !fields (11).isEmpty) fields (11).trim
              else null
              val addressType: String = if (fields (12) != null && !fields (12).isEmpty) fields (12).trim
              else null
              val zip: String = if (fields (13) != null && !fields (13).isEmpty) fields (13).trim
              else null
              val telephone: String = if (fields (14) != null && !fields (14).isEmpty) fields (14).trim
              else null
              val created: Date = TryParseDate (fields (15), inputDateFormat)
              val modified: Date = TryParseDate (fields (16), inputDateFormat)

              val customerAddress = new CustomerAddress (id, int2Integer (applicationId), customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified)
              customerAddress
            }
            val errorMessage: String = "%s %s %s".format ("Customer '%s' cannot be parsed due to invalid length of %s.", lineWithoutEscapeCharacters, fields.length)
            logger.error (errorMessage)
            null
          }
        }
        catch {
          case ex: Exception =>
            logger.error ("Failed to parse customer: " + line)
            null
        }

      }
    })
  }
}







