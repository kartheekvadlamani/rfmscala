package RFMCustomerAddressWriterFactory

import RFMCustomerAddress.CustomerAddress
import RFMCustomerAddressDataWriter.CustomerAddressDataWriter
import com.datastax.spark.connector.ColumnRef
import com.datastax.spark.connector.cql.TableDef
import com.datastax.spark.connector.writer.{RowWriter, RowWriterFactory}


/**
  * Created by kartheekvadlamani on 9/16/16.
  */
class CustomerAddressDataWriterFactory extends RowWriterFactory[CustomerAddress] {
  override def rowWriter(table: TableDef, selectedColumns: scala.IndexedSeq[ColumnRef]): RowWriter[CustomerAddress] = new CustomerAddressDataWriter
}

