package RFMOrder

import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

/**
  * Created by kartheekvadlamani on 9/12/16.
  */
class Order(id: Int, applicationId: Integer, orderId: String, orderNumber: String, status: String, financialStatus: String, customerIsGuest: Boolean, customerId: BigInteger, firstName: String, lastName: String, email: String, subTotalPrice: BigDecimal, totalDiscounts: BigDecimal, storeCredit: BigDecimal, totalPrice: BigDecimal, currencyCode: String, source: String, createdAt: Date,  updatedAt: Date, customerCreatedAt: Date, isSynced: Boolean, billingAddressId: String, shippingAddressId: String, created: Date, modified: Date, consumerOrderId: Integer, previousStatus: String, isPartialData: Boolean) extends Serializable{

  def getId: Int = id
  def getApplicationId: Integer = applicationId
  def getOrderId: String = orderId
  def getOrderNumber: String = orderNumber
  def getStatus: String = { if (status != null && !status.isEmpty) status.replaceAll("\\r", "") else status}
  def getFinancialStatus: String = financialStatus
  def getCustomerIsGuest: Boolean = customerIsGuest
  def getCustomerId: BigInteger = customerId
  def getFirstName: String = { if (firstName != null && !firstName.isEmpty) firstName.replaceAll("\\r", "") else firstName}
  def getLastName: String = { if (lastName != null && !lastName.isEmpty) lastName.replaceAll("\\r", "") else lastName}
  def getEmail: String = email
  def getSubTotalPrice: BigDecimal = subTotalPrice
  def getTotalDiscounts: BigDecimal = totalDiscounts
  def getStoreCredit: BigDecimal = storeCredit
  def getTotalPrice: BigDecimal = totalPrice
  def getCurrencyCode: String = currencyCode
  def getSource: String = source
  def getCreatedAt: Date = createdAt
  def getUpdatedAt: Date = updatedAt
  def getCustomerCreatedAt: Date = customerCreatedAt
  def getIsSynced: Boolean = isSynced
  def getBillingAddressId: String = billingAddressId
  def getShippingAddressId: String = shippingAddressId
  def getCreated: Date = created
  def getModified: Date = modified
  def getConsumerOrderId: Integer = consumerOrderId
  def getPreviousStatus: String = {if (previousStatus != null && !previousStatus.isEmpty) previousStatus.replaceAll("\\r", "") else previousStatus}
  def getIsPartialData: Boolean = isPartialData

  override def toString: String = {

    val outputDateFormatter: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
    val formattedCreatedAt: String = { if (createdAt != null) outputDateFormatter.format(createdAt) else null }
    val formattedUpdatedAt: String = { if (updatedAt != null) outputDateFormatter.format(updatedAt) else null}
    val formattedCustomerCreatedDate: String = {if (customerCreatedAt != null) outputDateFormatter.format(customerCreatedAt) else null}
    val formattedCreatedDate: String = { if (created != null) outputDateFormatter.format(created) else null}
    val formattedModifiedDate: String = { if (modified != null) outputDateFormatter.format(modified) else null}

    String.format("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, formattedCreatedAt, formattedUpdatedAt, formattedCustomerCreatedDate, isSynced, billingAddressId, shippingAddressId, formattedCreatedDate, formattedModifiedDate, consumerOrderId, previousStatus, isPartialData)
  }

}
