package RFMCustomerAddressDataWriter



import RFMCustomerAddress.CustomerAddress
import com.datastax.spark.connector.writer.RowWriter

/**
  * Created by kartheekvadlamani on 9/16/16.
  */
class CustomerAddressDataWriter extends RowWriter[CustomerAddress] {
  override val columnNames = scala.IndexedSeq ("application_id", "id", "customer_id", "first_name", "last_name", "company", "address1", "address2", "city", "province", "country", "province_code", "country_code", "address_type", "zip", "telephone", "created", "modified")

  override def readColumnValues(customerAddress: CustomerAddress, buffer: Array[Any]) = {
    buffer (0) = customerAddress.getApplicationId
    buffer (1) = customerAddress.getID
    buffer (2) = customerAddress.getCustomerId
    buffer (3) = customerAddress.getFirstName
    buffer (4) = customerAddress.getLastName
    buffer (5) = customerAddress.getCompany
    buffer (6) = customerAddress.getAddress1
    buffer (7) = customerAddress.getAddress2
    buffer (8) = customerAddress.getCity
    buffer (9) = customerAddress.getProvince
    buffer (10) = customerAddress.getCountry
    buffer (11) = customerAddress.getProvinceCode
    buffer (12) = customerAddress.getCountryCode
    buffer (13) = customerAddress.getAddressType
    buffer (14) = customerAddress.getZip
    buffer (15) = customerAddress.getTelephone
    buffer (16) = customerAddress.getCreated
    buffer (17) = customerAddress.getModified
  }
}
