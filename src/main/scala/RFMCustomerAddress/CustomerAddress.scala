package RFMCustomerAddress

import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

/**
  * Created by kartheekvadlamani on 9/13/16.
  */
class CustomerAddress(id: Int, applicationId: Integer, customerId: BigInteger, firstName: String, lastName: String, company: String, address1: String, address2: String, city: String, province: String, country: String, provinceCode: String, countryCode: String, addressType: String, zip: String, telephone: String, created: Date,
                      modified: Date) extends Serializable {
// def apply(id: Int, customerId: BigInteger, firstName: String, lastName: String, company: String, address1: String, address2: String, city: String, province: String, country: String, provinceCode: String, countryCode: String, addressType: String, zip: String, telephone: String, created: Date, modified: Date) = CustomerAddress (id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified)



  def CustomerAddress() = {}
   def getID: Int = id
   def getApplicationId:Integer = applicationId
   def getCustomerId: BigInteger = customerId
   def getFirstName: String = { if(firstName != null && !firstName.isEmpty) firstName.replaceAll("\\r", "") else firstName }
   def getLastName: String = { if(lastName != null && !lastName.isEmpty) lastName.replaceAll("\\r", "") else lastName }
   def getCompany: String = company
   def getAddress1: String = { if (address1 != null && !address1.isEmpty) address1.split("\\r")(0).replaceAll("\\r", "") else address1 }
   def getAddress2: String =  { if (address2 != null && !address2.isEmpty) address2.split("\\r")(0).replaceAll("\\r", "") else address2 }
   def getCity: String = city
   def getProvince: String = province
   def getCountry: String = country
   def getProvinceCode: String = provinceCode
   def getCountryCode: String = countryCode
   def getAddressType: String = addressType
   def getZip: String = {if (zip != null && !zip.isEmpty) zip.replaceAll("\\r", "") else zip}
   def getTelephone: String = telephone
   def getCreated: Date = created
   def getModified: Date = modified

  override def toString: String = {

    val outputDateFormatter: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
    val formattedCreatedDate: String = { if (created != null) outputDateFormatter.format(created) else null}
    val formattedModifiedDate: String = { if (modified != null) outputDateFormatter.format(modified) else null}

    String.format ("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s %s", id, applicationId, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, formattedCreatedDate, formattedModifiedDate)

  }

  def CustomerAddress(id: Int, customerId: BigInteger, firstName: String, lastName: String, company: String, address1: String, address2: String, city: String, province: String, country: String, provinceCode: String, countryCode: String, addressType: String, zip: String, telephone: String, created: Date,
                      modified: Date) = {
  }

}

