package RFMCustomerAddressConsumer

import java.math.BigInteger
import java.util

import RFMCustomerAddress.CustomerAddress
import kafka.consumer.{ConsumerIterator, KafkaStream}
import org.apache.log4j.Logger
import org.apache.spark.SparkContext
import java.util.{Date, List}

import RFMCustomerAddressWriterFactory.CustomerAddressDataWriterFactory
import com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions
import kafka.message.MessageAndMetadata
import org.apache.avro.generic.IndexedRecord
import org.apache.kafka.common.errors.SerializationException
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * Created by kartheekvadlamani on 9/13/16.
  */
class CustomerAddressConsumer(_kafkaStream: KafkaStream, _SparkContext: JavaSparkContext, _logger: Logger, _cassandraSchemaName: String, _cassandraTableName: String ) extends Runnable {

  override def run(): Unit = {

    val it: ConsumerIterator[_, _] = _kafkaStream.iterator
    while (it.hasNext) {

      val customerAddress: CustomerAddress = parseCustomerAddressFromStream(it)

      if (customerAddress != null) {

        val customerAddressList: util.ArrayList[CustomerAddress] = new util.ArrayList[CustomerAddress]
        customerAddressList.add(customerAddress)

        //Here Instead of using the JAVARDD i used a normal SCALA RDD.

       val customerAddressRdd: JavaRDD[CustomerAddress] = _SparkContext.parallelize(customerAddressList)

        // Save to Cassandra

        javaFunctions(customerAddressRdd).writerBuilder (_cassandraSchemaName, _cassandraTableName, new CustomerAddressDataWriterFactory).saveToCassandra ()

        _logger.debug("customerAddress: " + customerAddress.toString)

      }
    }
  }

def parseCustomerAddressFromStream(iterator: ConsumerIterator[_, _]) =

   try
{
  val messageAndMetadata: MessageAndMetadata[_, _] = iterator.next
  val key: String = messageAndMetadata.key.asInstanceOf [String]
  val value: IndexedRecord = messageAndMetadata.message.asInstanceOf [IndexedRecord]

  val applicationId: Integer = { if (value.get (0) != null) RFMHelpers.TryParseInteger(value.get (0).toString) else null }
  val id: Integer = { if (value.get (1) != null) RFMHelpers.TryParseInteger (value.get (1).toString) else null }
  val customerId: BigInteger = { if (value.get (2) != null) RFMHelpers.TryParseBigInteger(value.get(2).toString) else null }
  val firstName: String = { if (value.get (3) != null) value.get(3).toString else null}
  val lastName: String = { if (value.get(4)!= null) value.get(4).toString else null}
  val company: String = { if (value.get(5)!= null) value.get(5).toString else null}
  val address1: String = { if (value.get(6)!= null) value.get(6).toString else null}
  val address2: String = {if (value.get(7)!= null) value.get(7).toString else null}
  val city: String = { if (value.get(8)!= null) value.get(8).toString else null}
  val province: String = { if (value.get(9)!= null) value.get(9).toString else null}
  val country: String = { if (value.get(10)!= null) value.get(10).toString else null}
  val provinceCode: String = { if (value.get(11)!= null) value.get(11).toString else null}
  val countryCode: String = { if (value.get(12)!= null) value.get(12).toString else null}
  val addressType: String = { if (value.get(13)!= null) value.get(13).toString else null}
  val zip: String = { if (value.get(14)!= null) value.get(14).toString else null}
  val telephone: String = { if (value.get(15)!= null) value.get(15).toString else null}
  val created: Date = { if (value.get(16)!= null) new Date (value.get (16).asInstanceOf [Long]) else null}
  val modified: Date = { if (value.get(17)!= null) new Date (value.get (17).asInstanceOf [Long]) else null}

 val customerAddress: CustomerAddress = new CustomerAddress(Integer2int (applicationId), id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified)

  customerAddress
}

}


