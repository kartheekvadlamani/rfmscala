package RFMLastRun

import java.math.BigInteger

/**
  * Created by kartheekvadlamani on 10/5/16.
  */
class RFMLastRun(id: Integer, lastRunDate: String, lastRunCustomerId: BigInteger, lastRunCustomerAddressId: Integer, lastRunOrderId: Integer) {
  val _id = id
  val _lastRunDate = lastRunDate
  val _lastRunCustomerId = lastRunCustomerId
  val _lastRunCustomerAddressId = lastRunCustomerAddressId
  val _lastRunOrderId = lastRunOrderId

  def getId: Integer = _id
  def getlastRunDate: String = _lastRunDate
  def getlastRunCustomerId: BigInteger = _lastRunCustomerId
  def getlastRunCustomerAddressId: Integer = _lastRunCustomerAddressId
  def getlastRunOrderId: Integer = _lastRunOrderId
}
