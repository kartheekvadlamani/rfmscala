package RFMCustomerConsumer

import java.math.BigInteger
import java.util
import java.util.Date

import RFMCustomer.Customer
import RFMCustomerAddressWriterFactory.CustomerAddressDataWriterFactory
import RFMCustomerConsumerDataWriterFactory.CustomerConsumerDataWriterFactory
import com.datastax.spark.connector.japi.CassandraJavaUtil._
import kafka.consumer.{ConsumerIterator, KafkaStream}
import kafka.message.MessageAndMetadata
import org.apache.avro.generic.IndexedRecord
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.slf4j.Logger

/**
  * Created by kartheekvadlamani on 9/16/16.
  */
class CustomerConsumer(_kafkaStream: KafkaStream[_, _], _sparkContext: JavaSparkContext, _logger: Logger, _cassandraSchemaName: String, _cassandraTableName: String) extends Runnable {

override def run(): Unit = {

  val it: ConsumerIterator[_,_] = _kafkaStream.iterator()

  while (it.hasNext) {

    val customer: Customer = parseCustomerConsumerFromStream(it)

    if (customer != null) {

      val customerConsumerList: util.ArrayList[Customer] = new util.ArrayList[Customer]
    // customerConsumerList
      customerConsumerList.add(customer)

      val consumerCustomerRDD: JavaRDD[Customer] = _sparkContext.parallelize(customerConsumerList)

      // Save to Cassandra

      javaFunctions(consumerCustomerRDD).writerBuilder (_cassandraSchemaName, _cassandraTableName, new CustomerConsumerDataWriterFactory).saveToCassandra ()

      _logger.debug("customerConsumer: " + customer.toString)

    }
  }

  def parseCustomerConsumerFromStream(iterator: ConsumerIterator[_,_]): Customer = {

    val messageAndMetadata: MessageAndMetadata[_, _] = iterator.next
    val key: String = messageAndMetadata.key.asInstanceOf [String]
    val value: IndexedRecord = messageAndMetadata.message.asInstanceOf [IndexedRecord]
    val id: BigInteger = { if (value.get (0) != null) RFMHelpers.TryParseBigInteger (value.get (0).toString) else null }
    val applicationId: Integer = { if (value.get (1) != null) RFMHelpers.TryParseInteger (value.get (1).toString) else null }
    val customerId: BigInteger = { if (value.get (2) != null) RFMHelpers.TryParseBigInteger(value.get(2).toString) else null }
    val customerGroup: String = { if (value.get (3) != null) value.get(3).toString else null}
    val firstName: String = { if (value.get (4) != null) value.get(4).toString else null}
    val lastName: String = { if (value.get(5)!= null) value.get(5).toString else null}
    val email: String = { if (value.get(6)!= null) value.get(6).toString else null}
    val optInNewsletter: Boolean = { if (value.get(7)!= null) RFMHelpers.TryParseBoolean(value.get(7).toString) else null}
    val createdAt: Date = { if (value.get(8)!= null) new Date (value.get (8).asInstanceOf [Long]) else null}
    val updatedAt: Date = { if (value.get(9)!= null) new Date (value.get (9).asInstanceOf [Long]) else null}
    val isSynced: Boolean = { if (value.get (10) != null)  RFMHelpers.TryParseBoolean(value.get (10).asInstanceOf [Integer]) else Boolean2boolean (null)}
    val created: Date = { if (value.get(11)!= null) new Date (value.get (11).asInstanceOf [Long]) else null}
    val modified: Date = { if (value.get(12)!= null) new Date (value.get (12).asInstanceOf [Long]) else null}
    val consumerCustomerId: Integer = { if (value.get (13) != null) RFMHelpers.TryParseInteger (value.get (13).toString) else null}
    val isPartialData: Boolean = { if (value.get (14) != null)  RFMHelpers.TryParseBoolean(value.get (14).asInstanceOf [Integer]) else Boolean2boolean (null)}

    val customerConsumer: Customer = new Customer(id, Integer2int (applicationId), customerId, customerGroup, firstName, lastName, email, optInNewsletter, createdAt, updatedAt, isSynced, created, modified, consumerCustomerId, isPartialData)

    customerConsumer
  }
}


}
