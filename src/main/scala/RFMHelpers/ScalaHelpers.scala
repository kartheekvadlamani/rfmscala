package RFMHelpers

import java.math.{BigDecimal, BigInteger}
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

/**
  * Created by kartheekvadlamani on 9/23/16.
  */
class ScalaHelpers {

    def RemoveEscapeSequences(line: String): String = {

      // This is the escape sequence used to output the data (by default backslash escape commas in the string)
      // e.g. - sqoop --escaped-by \\ ...

      val escapeSequence: String = "\\,"
      var lineWithoutEscapeSequences: String = line

      if (line != null && !line.isEmpty && line.contains (escapeSequence)) {
        val regexEscapeSequence: String = "\\\\,"
        lineWithoutEscapeSequences = line.replaceAll (regexEscapeSequence, "")
      }
      lineWithoutEscapeSequences

    }


    def TryParseBigDecimal(decimalString: String): BigDecimal = {

      var decimal: BigDecimal = null
      val nullString: String = new String ("null")

      if (decimalString != null && !decimalString.isEmpty && decimalString != nullString) {

        decimal = new BigDecimal (decimalString)
      }
      decimal

    }


    def TryParseBigInteger(bigIntValue: String): BigInteger = {

      var bigInteger: BigInteger = null
      val nullString: String = new String ("null")

      if (bigIntValue != null && !bigIntValue.isEmpty && bigIntValue != nullString) {

        bigInteger = new BigInteger (bigIntValue)
      }

      bigInteger
    }


    def TryParseInteger(integerValue: String): Integer = {

      var integer: Integer = null
      val nullString: String = new String ("null")

      if (integerValue != null && !integerValue.isEmpty && integerValue != nullString) {

        integer = augmentString (integerValue).toInt

      }

      integer
    }


    def TryParseBoolean(boolValue: String): Boolean = {

      var bool: Boolean = false
      val nullString: String = new String ("null")

      if (boolValue != null && !boolValue.isEmpty && boolValue != nullString) {
        bool = boolValue.toBoolean
      }
      bool
    }


    def TryParseDate(dateValue: String, dateFormat: SimpleDateFormat): Date = {


      var date: Date = null
      val simpleDateFormat: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)

      val nullString: String = new String ("null")

      if (dateValue != null && !dateValue.isEmpty && dateValue != nullString) date = simpleDateFormat.parse (dateValue)
      date
    }

  }

