package RFMAddShutDownHook

import org.apache.spark.api.java.JavaSparkContext

/**
  * Created by kartheekvadlamani on 9/15/16.
  */
class AddShutDownHook(sparkContext: JavaSparkContext) {

  val _sparkContext = sparkContext

  def attachShutDownHook(): Unit =  {
    Runtime.getRuntime.addShutdownHook (new Thread () {
      override def run() {
        if (_sparkContext != null) {
          _sparkContext.stop
        }
      }
    })
  }
}
