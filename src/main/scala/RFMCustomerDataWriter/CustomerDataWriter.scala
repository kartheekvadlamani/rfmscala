package RFMCustomerDataWriter

import RFMCustomer.Customer
import com.datastax.spark.connector.writer.RowWriter

/**
  * Created by kartheekvadlamani on 9/17/16.
  */
class CustomerDataWriter extends RowWriter[Customer]{

  override val columnNames = scala.IndexedSeq ("id", "applicationId", "customerId" , "customerGroup", "firstName", "lastName", "email", "optInNewsletter", "createdAt", "updatedAt", "isSynced", "created", "modified", "consumerCustomerId" , "isPartialData")

  override def readColumnValues(customer: Customer, buffer: Array[Any]) = {

    buffer (1) = customer.getId
    buffer (0) = customer.getApplicationId
    buffer (2) = customer.getCustomerId
    buffer (3) = customer.getCustomerGroup
    buffer (4) = customer.getFirstName
    buffer (5) = customer.getLastName
    buffer (6) = customer.getEmail
    buffer (7) = customer.getOptInNewsletter
    buffer (8) = customer.getCreatedAt
    buffer (9) = customer.getUpdatedAt
    buffer (10) = customer.getIsSynced
    buffer (11) = customer.getCreated
    buffer (12) = customer.getModified
    buffer (13) = customer.getConsumerCustomerId
    buffer (14) = customer.getIsPartialData

  }

}
