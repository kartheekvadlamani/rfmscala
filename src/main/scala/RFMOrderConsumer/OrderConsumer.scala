package RFMOrderConsumer

import java.math.{BigDecimal, BigInteger}
import java.util
import java.util.Date

import RFMOrder.Order
import RFMHelpers.ScalaHelpers
import com.datastax.spark.connector.japi.CassandraJavaUtil._
import kafka.consumer.{ConsumerIterator, KafkaStream}
import kafka.message.MessageAndMetadata
import org.apache.avro.generic.IndexedRecord
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.slf4j.Logger

/**
  * Created by kartheekvadlamani on 9/22/16.
  */
class OrderConsumer(_kafkaStream: KafkaStream[_, _], sparkContext: JavaSparkContext, logger: Logger, cassandraSchemaName: String, cassandraTableName: String) extends Runnable {

def OrderConsumer() = { }

  def Run: Void = {

    val it: ConsumerIterator[_, _] = _kafkaStream.iterator()

    while(it.hasNext()) {
      val order: Order = parseOrderFromStream(it)

      if (order != null) {
        val orders: util.ArrayList[Order] = new util.ArrayList[Order]
        orders.add(order)

        val ordersRDD: JavaRDD[Order] = sparkContext.parallelize(orders)

        // Save to Cassandra
        javaFunctions (ordersRDD).writerBuilder (cassandraSchemaName, cassandraTableName, new OrderWriterFactory).saveToCassandra ()

        logger.debug ("order: " + order.toString)
      }
    }
  }

  def parseOrderFromStream(consumerIterator: ConsumerIterator): Order = {

    try {
      val messageAndMetadata: MessageAndMetadata[_, _] = consumerIterator.next
      val key: String = messageAndMetadata.key.asInstanceOf [String]
      val value: IndexedRecord = messageAndMetadata.message.asInstanceOf [IndexedRecord]


        val id: Int = if (value.get (0) != null) ScalaHelpers.TryParseInteger(value.get (0).toString) else null
        val applicationId: Int = if (value.get (0) != null) ScalaHelpers.TryParseInteger(value.get (0).toString) else null
        val orderId: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val orderNumber: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val status: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val financialStatus: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val customerIsGuest: Boolean = ScalaHelpers.TryParseBoolean (value.get (6))
        val customerId: BigInteger = ScalaHelpers.TryParseBigInteger (value.get (7))
        val firstName: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val lastName: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val email: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val subTotalPrice: BigDecimal = ScalaHelpers.TryParseBigDecimal (value.get (11))
        val totalDiscounts: BigDecimal = ScalaHelpers.TryParseBigDecimal (value.get (12))
        val storeCredit: BigDecimal = ScalaHelpers.TryParseBigDecimal (value.get (13))
        val totalPrice: BigDecimal = ScalaHelpers.TryParseBigDecimal (value.get (14))
        val currencyCode: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val source: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val createdAt: Date = TryParseDate (value.get (17), inputDateFormat)
        val updatedAt: Date = TryParseDate (value.get (18), inputDateFormat)
        val customerCreatedAt: Date = TryParseDate (value.get (19), inputDateFormat)
        val isSynced: Boolean = TryParseBoolean (value.get (20))
        val billingAddressId: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val shippingAddressId: String = if (value.get (0) != null)  value.get (0).toString
        else null
        val created: Date = TryParseDate (value.get (23), inputDateFormat)
        val modified: Date = TryParseDate (value.get (24), inputDateFormat)
        val consumerOrderId: Integer = TryParseInteger (value.get (25))
        val previousStatus: String = if (value.get (26) != null && !value.get (26).isEmpty) value.get (26).trim
        else null
        val isPartialData: Boolean = TryParseBoolean (value.get (27))
        val order: Order = new Order (id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt, isSynced, billingAddressId, shippingAddressId, created, modified, consumerOrderId, previousStatus, isPartialData)
        order

    }
      catch {
        case ex: Exception =>
          logger.error ("Failed to parse order: " + "serialized exception")
          null
    }
  }
}
