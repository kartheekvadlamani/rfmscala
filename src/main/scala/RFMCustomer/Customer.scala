package RFMCustomer

import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

/**
  * Created by kartheekvadlamani on 9/12/16.
  */
class Customer(id: BigInteger, applicationId: Int, customerId: BigInteger, customerGroup: String, firstName: String, lastName: String, email: String, optInNewsletter: Boolean, createdAt: Date, updatedAt: Date, isSynced: Boolean, created: Date, modified: Date, consumerCustomerId: Integer, isPartialData: Boolean) extends Serializable{

  def Customer() = {}
  def getId: BigInteger = id

  def getApplicationId: Int = applicationId

  def getCustomerId: BigInteger = customerId

  def getCustomerGroup: String = {
    if (customerGroup != null && !customerGroup.isEmpty) customerGroup.replaceAll ("\\r", "")
    else customerGroup
  }

  def getFirstName: String = {
    if (firstName != null && !firstName.isEmpty) firstName.replaceAll ("\\r", "")
    else firstName
  }

  def getLastName: String = {
    if (lastName != null && !lastName.isEmpty) lastName.replaceAll ("\\r", "")
    else lastName
  }

  def getEmail: String = {
    if (email != null && !email.isEmpty) email.replaceAll ("\\r", "")
    else email
  }

  def getOptInNewsletter: Boolean = optInNewsletter

  def getCreatedAt: Date = createdAt

  def getUpdatedAt: Date = updatedAt

  def getIsSynced: Boolean = isSynced

  def getCreated: Date = created

  def getModified: Date = modified

  def getConsumerCustomerId: Integer = consumerCustomerId

  def getIsPartialData: Boolean = isPartialData

 override def toString: String = {
    val outputDateFormatter: SimpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss", Locale.US)
    val formattedCreatedAt: String = { if (createdAt != null) outputDateFormatter.format(createdAt) else null }
    val formattedUpdatedAt: String = { if (updatedAt!= null) outputDateFormatter.format(updatedAt) else null}
    val formattedCreated: String = { if (created != null) outputDateFormatter.format(created) else null}
    val formattedModified: String = { if (modified != null) outputDateFormatter.format(modified) else null}

    String.format("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s ", id, applicationId, customerId, customerGroup, firstName, lastName, email, optInNewsletter, formattedCreatedAt, formattedUpdatedAt, isSynced, formattedCreated, formattedModified, consumerCustomerId, isPartialData)

  }
}
