package RFMCassandra

import java.math.BigInteger
import java.util
import java.util.{Comparator, Date}

import RFMCustomer.Customer
import RFMCustomerAddress.CustomerAddress
import RFMLastRun.RFMLastRun
import RFMOrder.Order
import com.datastax.driver.core.{ResultSet, Row, Session, TypeCodec}
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.slf4j.Logger

import scala.collection.JavaConversions._

/**
  * Created by kartheekvadlamani on 9/26/16.
  */
class Cassandra {

  def IntializeBatchLayerTables(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRFMResultsTableName: String, cassandraSummaryStatisticsTableName: String, cassandraTop1PercentSummaryStatisticsTableName: String, cassandraTop5PercentSummaryStatisticsTableName: String, cassandraTop15PercentSummaryStatisticsTableName: String,
                                cassandraTop25PercentSummaryStatisticsTableName: String, cassandraTop50PercentSummaryStatisticsTableName: String, cassandraBottom50PercentSummaryStatisticsTableName: String, cassandraRfmLastRunTableName: String): Unit = {

    var session: Session = null

    try {
      val connector: CassandraConnector = CassandraConnector.apply (javaSparkContext.getConf)

      session = connector.openSession ()

      session.execute (String.format ("CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}", cassandraSchemaName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s (application_id INT, customer_id VARINT, customer_name TEXT, company_name TEXT, customer_group TEXT, customer_city TEXT, customer_state TEXT, customer_country TEXT, customer_email TEXT, " + "orders_sub_total DECIMAL, orders_count INT, first_order_date TIMESTAMP, last_order_date TIMESTAMP, average_days_between_orders INT, first_order_amount DECIMAL, last_order_amount DECIMAL, average_order_price DECIMAL, customer_created_at TIMESTAMP, " + "PRIMARY KEY (application_id, customer_id))", cassandraSchemaName, cassandraRFMResultsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " + "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " + "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop1PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraTop5PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraTop15PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraTop25PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraTop50PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraBottom50PercentSummaryStatisticsTableName))
      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(id INT PRIMARY KEY, last_run_date TEXT, last_run_customer_id BIGINT, last_run_customer_address_id INT, last_run_order_id INT)", cassandraSchemaName, cassandraRfmLastRunTableName))

    }
    finally {

      if (session != null && !session.isClosed) {
        session.close ()
      }
    }
  }

  def DeleteFromSpeedLayerTable(session: Session, cassandraSchemaName: String, cassandraTableName: String, applicationId: Integer): Unit = {
    session.execute (String.format ("DELETE FROM %s %s WHERE application_id = %s", cassandraSchemaName, cassandraTableName, applicationId))

  }

  def InitializeSpeedLayerTables(javaSparkContext: JavaSparkContext, logger: Logger, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, cassandraCustomersTableName: String, cassandraCustomerAddressesTableName: String, cassandraOrdersTableName: String, cassandraRFMResultsTableName: String, cassandraSummaryStatisticsSpeedTableName: String, cassandraTop1PercentSummaryStatisticsSpeedTableName: String, cassandraTop5PercentSummaryStatisticsSpeedTableName: String, cassandraTop15PercentSummaryStatisticsSpeedTableName: String,
                                 cassandraTop25PercentSummaryStatisticsSpeedTableName: String, cassandraTop50PercentSummaryStatisticsSpeedTableName: String, cassandraBottom50PercentSummaryStatisticsSpeedTableName: String): Unit = {

    var session: Session = null

    try {

      val connector: CassandraConnector = CassandraConnector.apply (javaSparkContext.getConf)

      session = connector.openSession ()

      session.execute (String.format ("CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}", cassandraSchemaName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s (id BIGINT, application_id INT, customer_id BIGINT, customer_group TEXT, first_name TEXT, last_name TEXT, email TEXT, opt_in_newsletter BOOLEAN, created_at TIMESTAMP, updated_at TIMESTAMP, is_synced BOOLEAN," +
        "created TIMESTAMP, modified TIMESTAMP, consumer_customer_id INT, is_partial_data BOOLEAN, PRIMARY KEY (id))", cassandraSchemaName, cassandraCustomersTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s (application_id INT, id INT, customer_id BIGINT, first_name TEXT, last_name TEXT, company TEXT, address1 TEXT, address2 TEXT, city TEXT, province TEXT, country TEXT, province_code TEXT," +
        "country_code TEXT, address_type TEXT, zip TEXT, telephone TEXT, created TIMESTAMP, modified TIMESTAMP, PRIMARY KEY (id))", cassandraSchemaName, cassandraCustomerAddressesTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s (id INT, application_id INT, order_id TEXT, order_number TEXT, status TEXT, financial_status TEXT, customer_is_guest BOOLEAN, customer_id BIGINT, first_name TEXT, last_name TEXT," + "email TEXT, sub_total_price DECIMAL, total_discounts DECIMAL, store_credit DECIMAL, total_price DECIMAL, currency_code TEXT, source TEXT, created_at TIMESTAMP, updated_at TIMESTAMP, customer_created_at TIMESTAMP, is_synced BOOLEAN, billing_address_id TEXT, " +
        "shipping_address_id TEXT, created TIMESTAMP, modified TIMESTAMP, consumer_order_id INT, previous_status TEXT, is_partial_data BOOLEAN, PRIMARY KEY (id))", cassandraSchemaName, cassandraOrdersTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s (application_id INT, customer_id VARINT, customer_name TEXT, company_name TEXT, customer_group TEXT, customer_city TEXT, customer_state TEXT, customer_country TEXT, customer_email TEXT, " + "orders_sub_total DECIMAL, orders_count INT, first_order_date TIMESTAMP, last_order_date TIMESTAMP, average_days_between_orders INT, first_order_amount DECIMAL, last_order_amount DECIMAL, average_order_price DECIMAL, customer_created_at TIMESTAMP, " +
        "PRIMARY KEY (application_id, customer_id))", cassandraSchemaName, cassandraRFMResultsTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName,
        cassandraSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop1PercentSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop5PercentSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop15PercentSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop25PercentSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraTop50PercentSummaryStatisticsSpeedTableName))

      session.execute (String.format ("CREATE TABLE IF NOT EXISTS %s %s(application_id INT PRIMARY KEY, total_customers BIGINT, total_amount_of_orders DECIMAL, percentage_total_amount_of_orders DECIMAL, average_order_value DECIMAL, average_customer_lifetime_value DECIMAL, " +
        "total_number_of_orders INT, percentage_total_number_of_orders DECIMAL, average_number_of_orders DECIMAL, percentage_of_repeat_buyers DECIMAL, percentage_revenue_from_repeat_buyers DECIMAL, average_days_since_last_order INT)", cassandraSchemaName, cassandraBottom50PercentSummaryStatisticsSpeedTableName))

      CleanupOldBatchLayerDataFromSpeedLayerTables (javaSparkContext, logger, null, cassandraSchemaName, cassandraRfmLastRunTableName, cassandraCustomersTableName, cassandraCustomerAddressesTableName, cassandraOrdersTableName, cassandraRFMResultsTableName, cassandraSummaryStatisticsSpeedTableName, cassandraTop1PercentSummaryStatisticsSpeedTableName, cassandraTop5PercentSummaryStatisticsSpeedTableName, cassandraTop15PercentSummaryStatisticsSpeedTableName, cassandraTop25PercentSummaryStatisticsSpeedTableName, cassandraTop50PercentSummaryStatisticsSpeedTableName, cassandraBottom50PercentSummaryStatisticsSpeedTableName)
    }

    finally {

      if (session != null && !session.isClosed) {

        session.close ()
      }
    }
  }

  def cleanUpInvalidApplicationsFromSpeedLayerResultsTables (invalidApplicationIds: List[Integer], cassandraSchemaName: String, cassandraRFMSpeedResultsTableName: String, cassandraSummaryStaticsSpeedTableName: String, cassandraTop1PercentSummaryStatisticsSpeedTableName: String, cassandraTop5PercentSummaryStatisticsSpeedTableName: String, cassandraTop15PercentSummaryStatisticsSpeedTableName: String, cassandraTop25PercentSummaryStatisticsSpeedTableName: String, cassandraTop50PercentSummaryStatisticsSpeedTableName: String, cassandraBottom50PercentSummaryStatisticsSpeedTableName: String, session: Session): Unit = {
    for (applicationId <- invalidApplicationIds) {
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraRFMSpeedResultsTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraSummaryStaticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraTop1PercentSummaryStatisticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraTop5PercentSummaryStatisticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraTop15PercentSummaryStatisticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraTop25PercentSummaryStatisticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraTop50PercentSummaryStatisticsSpeedTableName, applicationId)
      DeleteFromSpeedLayerTable (session, cassandraSchemaName, cassandraBottom50PercentSummaryStatisticsSpeedTableName, applicationId)
    }
  }

  def CleanupOldBatchLayerDataFromSpeedLayerTables(javaSparkContext: JavaSparkContext, logger: Logger, invalidApplicationIds: List[Integer], cassandraSchemaName: String, cassandraRfmLastRunTableName: String, cassandraCustomersTableName: String, cassandraCustomerAddressesTableName: String, cassandraOrdersTableName: String, cassandraRFMResultsTableName: String, cassandraSummaryStatisticsSpeedTableName: String, cassandraTop1PercentSummaryStatisticsSpeedTableName: String, cassandraTop5PercentSummaryStatisticsSpeedTableName: String, cassandraTop15PercentSummaryStatisticsSpeedTableName: String, cassandraTop25PercentSummaryStatisticsSpeedTableName: String, cassandraTop50PercentSummaryStatisticsSpeedTableName: String, cassandraBottom50PercentSummaryStatisticsSpeedTableName: String): Unit = {

    var session: Session = null

    try {

      logger.info ("Cleaning up old speed layer customer, addresses and orders from Cassandra.")

      val connector: CassandraConnector = CassandraConnector.apply (javaSparkContext.getConf)

      session = connector.openSession ()

      // Get last customer id, customer address id, order id
      val lastRunCustomerId: BigInteger = GetLastRunCustomerId (javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
      val lastRunCustomerAddressId: Integer = GetLastRunCustomerAddressId (javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
      val lastRunOrderId: Integer = GetLastRunOrderId (javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)

      // Delete old customers, addresses and orders from Cassandra that have already been processed by the batch layer
      if (lastRunCustomerId != null) {
        deleteOldCustomers (javaSparkContext, cassandraSchemaName, cassandraCustomersTableName, session, lastRunCustomerId)
      }

      if (lastRunCustomerAddressId != null) {
        deleteOldCustomerAddresses (javaSparkContext, cassandraSchemaName, cassandraCustomerAddressesTableName, session, lastRunCustomerAddressId)
      }

      if (lastRunOrderId != null) {
        deleteOldOrders (javaSparkContext, cassandraSchemaName, cassandraOrdersTableName, session, lastRunOrderId)
      }

      if (invalidApplicationIds != null && invalidApplicationIds.nonEmpty) {
       // cleanUpInvalidApplicationsFromSpeedLayerResultsTables (invalidApplicationIds, cassandraSchemaName, cassandraRFMSpeedResultsTableName, cassandraSummaryStatisticsSpeedTableName, cassandraTop1PercentSummaryStatisticsSpeedTableName, cassandraTop5PercentSummaryStatisticsSpeedTableName, cassandraTop15PercentSummaryStatisticsSpeedTableName, cassandraTop25PercentSummaryStatisticsSpeedTableName, cassandraTop50PercentSummaryStatisticsSpeedTableName, cassandraBottom50PercentSummaryStatisticsSpeedTableName, session)
        cleanUpInvalidApplicationsFromSpeedLayerResultsTables(invalidApplicationIds,cassandraSchemaName, cassandraRFMSpeedResultsTableName = String, cassandraSummaryStatisticsSpeedTableName, cassandraTop1PercentSummaryStatisticsSpeedTableName, cassandraTop5PercentSummaryStatisticsSpeedTableName, cassandraTop15PercentSummaryStatisticsSpeedTableName,cassandraTop25PercentSummaryStatisticsSpeedTableName, cassandraTop50PercentSummaryStatisticsSpeedTableName, cassandraBottom50PercentSummaryStatisticsSpeedTableName = String, session)
      }
    }
    finally {
      if (session != null && !session.isClosed) {
        session.close ()
      }
    }
  }


  def deleteOldOrders(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraOrdersTableName: String, session: Session, lastRunOrderId: Integer): Unit = {
    val orders: util.List[Order] = getSpeedLayerOrders(session, cassandraSchemaName, cassandraOrdersTableName)
    if (orders != null && orders.nonEmpty) {
      val orderJavaRDD: JavaRDD[Order] = javaSparkContext.parallelize (orders)
      val oldOrders: util.List[Order] = getFilteredOrders (orderJavaRDD, lastRunOrderId, false).takeOrdered(1000000)

      // Delete old orders from Cassandra
      for (oldOrder <- oldOrders) {
        if (oldOrder != null) {
          session.execute (String.format ("DELETE FROM %s %s WHERE ID = %s", cassandraSchemaName, cassandraOrdersTableName, oldOrder.getId))
        }
      }
    }
  }

  def deleteOldCustomers(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraCustomersTableName: String, session: Session, lastRunCustomerId: BigInteger): Unit = {

    val customers: util.List[Customer] = getSpeedLayerCustomers(session, cassandraSchemaName, cassandraCustomersTableName)
    if (customers != null && customers.nonEmpty) {
      val customersRDD: JavaRDD[Customer] = javaSparkContext.parallelize (customers)
      val oldCustomers: util.List[Customer] = getFilteredCustomers (customersRDD, lastRunCustomerId, false).takeOrdered(1000000)

      //Delete old Customers from Cassandra
      for (oldCustomer <- oldCustomers) {
        if (oldCustomer != null) {

          session.execute (String.format ("DELETE FROM %s %s WHERE ID = %s", cassandraSchemaName, cassandraCustomersTableName, oldCustomer.getId))
        }
      }
    }
  }

  def deleteOldCustomerAddresses(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraCustomerAddressTableName: String, session: Session, lastRunCustomerAddressId: Integer): Unit = {

    val customerAddresses: util.List[CustomerAddress] = getSpeedLayerCustomerAddresses(session, cassandraSchemaName, cassandraCustomerAddressTableName)
    if (customerAddresses != null && customerAddresses.nonEmpty) {
      val customerAddressRDD: JavaRDD[CustomerAddress] = javaSparkContext.parallelize (customerAddresses)
      val oldCustomerAddresses: util.List[CustomerAddress] = getFilteredCustomerAddresses (customerAddressRDD, lastRunCustomerAddressId, false).takeOrdered(1000000)

      //Delete old Customers from Cassandra
      for (oldCustomerAddress <- oldCustomerAddresses) {
        if (oldCustomerAddress != null) {

          session.execute (String.format ("DELETE FROM %s %s WHERE ID = %s", cassandraSchemaName, cassandraCustomerAddressTableName, oldCustomerAddress.getID))
        }
      }
    }
  }

  def GetLastRunCustomerId(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRfmLastRunTableName: String): BigInteger = {

    val rfmLastRuns: util.List[RFMLastRun] = GetLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
    val lastRunCustomerId: BigInteger = null

    if (rfmLastRuns != null && rfmLastRuns.nonEmpty) {

      val rfmLastRun: RFMLastRun = rfmLastRuns.get (rfmLastRuns.size - 1)
      val lastRunCustomerId: BigInteger = rfmLastRun.getlastRunCustomerId
    }

    lastRunCustomerId

  }

  def GetLastRunCustomerAddressId(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRfmLastRunTableName: String): Integer = {

    val rfmLastRuns: util.List[RFMLastRun] = GetLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
    val lastRunCustomerAddressId: Integer = null

    if (rfmLastRuns != null && rfmLastRuns.nonEmpty) {

      val rfmLastRun: RFMLastRun = rfmLastRuns.get (rfmLastRuns.size - 1)
      val lastRunCustomerAddressId: Integer = rfmLastRun.getlastRunCustomerAddressId
    }

    lastRunCustomerAddressId
  }

  def GetLastRunOrderId(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRfmLastRunTableName: String): Integer = {

    val rfmLastRuns: util.List[RFMLastRun] = GetLastRFMRuns(javaSparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
    val lastRunOrderId: Integer = null

    if (rfmLastRuns != null && rfmLastRuns.nonEmpty) {

      val rfmLastRun: RFMLastRun = rfmLastRuns.get (rfmLastRuns.size - 1)
      val lastRunCustomerAddressId: Integer = rfmLastRun.getlastRunOrderId
    }

    lastRunOrderId
  }

  def GetLastRFMRuns(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRfmLastRunTableName: String): util.List[RFMLastRun] = {

    val session: Session = null
    val runLastRunList: util.List[RFMLastRun] = new util.ArrayList[RFMLastRun]

    try {
      val connector: CassandraConnector = CassandraConnector.apply (javaSparkContext.getConf)
      val session = connector.openSession ()
      val lastRunDateset: ResultSet = session.execute (String.format ("SELECT id, last_run_date, last_run_customer_id, last_run_customer_address_id, last_run_order_id FROM %s %s", cassandraSchemaName, cassandraRfmLastRunTableName))

      if (lastRunDateset != null) {
        val allDates: util.List[Row] = lastRunDateset.all ()
        for (row: Row <- allDates) {
          val id: Integer = row.getInt (0)
          val lastRunDate: String = row.getString (1)
          val lastRunCustomerId: BigInteger = BigInteger.valueOf (row.get (2, TypeCodec.bigint))
          val lastRunCustomerAddressId: Integer = row.getInt (3)
          val lastRunOrderId: Integer = row.getInt (4)

          val rfmLastRun: RFMLastRun = new RFMLastRun (id, lastRunDate, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId)

          runLastRunList.add (rfmLastRun)
        }

        runLastRunList.sort (new Comparator[RFMLastRun]() {
          def compare(rfmLastRun1: Row, rowLastRun2: Row) = {
            val rfmLastRun1Id: BigInteger = new BigInteger (rfmLastRun1.get(0,TypeCodec.bigint()).toString)
            val rfmLastRun2Id: BigInteger = new BigInteger (rowLastRun2.get(0,TypeCodec.bigint()).toString)
            rfmLastRun1Id.compareTo (rfmLastRun2Id)
          }

          override def compare(o1: RFMLastRun, o2: RFMLastRun): Int = 0
        })

      }
    }
    finally {
      if (session != null && !session.isClosed) {
        session.close ()
      }
    }
    runLastRunList
  }

  def getSpeedLayerCustomers(session: Session, cassandraSchemaName: String, customersTableName: String): util.List[Customer] = {
    val customersSet: ResultSet = session.execute (String.format ("SELECT id, application_id, customer_id, customer_group, first_name, last_name, email, opt_in_newsletter, created_at, updated_at, is_synced, created, modified, consumer_customer_id, is_partial_data FROM %s %s", cassandraSchemaName, customersTableName))
    val customers: util.List[Customer] = new util.ArrayList[Customer]()
    if (customersSet != null) {
      for (customerRow: Row <- customersSet.all) {
        val id: BigInteger = if (customerRow.get (0, TypeCodec.bigint) != null) BigInteger.valueOf (customerRow.get (0, TypeCodec.bigint)) else null
        val applicationId: Integer = customerRow.get (1, classOf [Integer])
        val customerId: BigInteger = if (customerRow.get (2, TypeCodec.bigint) != null) BigInteger.valueOf (customerRow.get (2, TypeCodec.bigint)) else null
        val customerGroup: String = customerRow.getString (3)
        val firstName: String = customerRow.getString (4)
        val lastName: String = customerRow.getString (5)
        val email: String = customerRow.getString (6)
        val optInNewsletter: Boolean = customerRow.get (7, classOf [Boolean])
        val createdAt: Date = customerRow.getTimestamp (8)
        val updatedAt: Date = customerRow.getTimestamp (9)
        val isSynced: Boolean = customerRow.get (10, classOf [Boolean])
        val created: Date = customerRow.getTimestamp (11)
        val modified: Date = customerRow.getTimestamp (12)
        val consumerCustomerId: Integer = customerRow.get (13, classOf [Integer])
        val isPartialData: Boolean = customerRow.get (14, classOf [Boolean])

        val customer: Customer = new Customer (id, applicationId, customerId, customerGroup, firstName, lastName, email, optInNewsletter, createdAt, updatedAt, isSynced, created, modified, consumerCustomerId, isPartialData)

        customers.add (customer)
      }
    }
    customers
  }

  def getSpeedLayerCustomerAddresses(session: Session, cassandraSchemaName: String, customerAddressesTableName: String): util.List[CustomerAddress] = {
    val customerAddressesSet: ResultSet = session.execute (String.format ("SELECT application_id, id, customer_id, first_name, last_name, company, address1, address2, city, province, country, province_code, country_code, address_type, zip, telephone, created, modified FROM %s %s", cassandraSchemaName, customerAddressesTableName))
    val customerAddresses: util.List[CustomerAddress] = new util.ArrayList[CustomerAddress]()
    if (customerAddressesSet != null) {
      for (customerAddressRow: Row <- customerAddressesSet.all ()) {
        val applicationId: Integer = customerAddressRow.get(0, classOf[Integer])
        val id: Integer = customerAddressRow.get(1, classOf [Integer])
        val customerId: BigInteger = if (customerAddressRow.get (2, TypeCodec.bigint) != null) BigInteger.valueOf (customerAddressRow.get (2, TypeCodec.bigint)) else null
        val firstName: String = customerAddressRow.getString (3)
        val lastName: String = customerAddressRow.getString (4)
        val company: String = customerAddressRow.getString (5)
        val address1: String = customerAddressRow.getString (6)
        val address2: String = customerAddressRow.getString (7)
        val city: String = customerAddressRow.getString (8)
        val province: String = customerAddressRow.getString (9)
        val country: String = customerAddressRow.getString (10)
        val provinceCode: String = customerAddressRow.getString (11)
        val countryCode: String = customerAddressRow.getString (12)
        val addressType: String = customerAddressRow.getString (13)
        val zip: String = customerAddressRow.getString (14)
        val telephone: String = customerAddressRow.getString (15)
        val created: Date = customerAddressRow.getTimestamp (16)
        val modified: Date = customerAddressRow.getTimestamp (17)

        val customerAddress: CustomerAddress = new CustomerAddress (applicationId, id, customerId, firstName, lastName, company, address1, address2, city, province, country, provinceCode, countryCode, addressType, zip, telephone, created, modified)

        customerAddresses.add(customerAddress)
      }
    }

    customerAddresses
  }

  def getSpeedLayerOrders(session: Session, cassandraSchemaName: String, ordersTableName: String): util.List[Order] = {
    val ordersSet: ResultSet = session.execute (String.format ("SELECT id, application_id, order_id, order_number, status, financial_status, customer_is_guest, customer_id, first_name, last_name, email, sub_total_price, total_discounts, store_credit, total_price, currency_code, source," + "created_at, updated_at, customer_created_at, is_synced, billing_address_id, shipping_address_id, created, modified, consumer_order_id, previous_status, is_partial_data FROM %s %s", cassandraSchemaName, ordersTableName))
    val orders: util.List[Order] = new util.ArrayList[Order]()
    if (ordersSet != null) {
      for (orderRow: Row <- ordersSet.all()) {
        val id: Integer = orderRow.get (0, classOf [Integer])
        val applicationId: Integer = orderRow.get (1, classOf [Integer])
        val orderId: String = orderRow.getString (2)
        val orderNumber: String = orderRow.getString (3)
        val status: String = orderRow.getString (4)
        val financialStatus: String = orderRow.getString (5)
        val customerIsGuest: Boolean = orderRow.get (6, classOf [Boolean])
        val customerId:BigInteger = if (orderRow.get (7, TypeCodec.bigint) != null) BigInteger.valueOf (orderRow.get (7, TypeCodec.bigint)) else null
        val firstName: String = orderRow.getString (8)
        val lastName: String = orderRow.getString (9)
        val email: String = orderRow.getString (10)
        val subTotalPrice: BigDecimal = orderRow.get (11, TypeCodec.decimal)
        val totalDiscounts: BigDecimal = orderRow.get (12, TypeCodec.decimal)
        val storeCredit: BigDecimal = orderRow.get (13, TypeCodec.decimal)
        val totalPrice: BigDecimal = orderRow.get (14, TypeCodec.decimal)
        val currencyCode: String = orderRow.getString (15)
        val source: String = orderRow.getString (16)
        val createdAt: Date = orderRow.getTimestamp (17)
        val updatedAt: Date = orderRow.getTimestamp(18)
        val customerCreatedAt: Date = orderRow.getTimestamp(19)
        val isSynced: Boolean = orderRow.get (20, classOf [Boolean])
        val billingAddressId: String = orderRow.getString (21)
        val shippingAddressId: String = orderRow.getString (22)
        val created: Date = orderRow.getTimestamp (23)
        val modified: Date = orderRow.getTimestamp (24)
        val consumerOrderId: Integer = orderRow.get (25, classOf [Integer])
        val previousStatus: String = orderRow.getString (26)
        val isPartialData: Boolean = orderRow.get (27, classOf [Boolean])

        val order: Order = new Order (id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt, isSynced, billingAddressId, shippingAddressId, created, modified, consumerOrderId, previousStatus, isPartialData)

        orders.add(order)

      }
    }

    orders

  }

def CleanupCassandra(javaSparkContext: JavaSparkContext, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, currentDateTimePath: String, lastRunCustomerId: BigInteger, lastRunCustomerAddressId: Integer, lastRunOrderId: Integer): Unit = {


  val session: Session = null
  try {
    val connector: CassandraConnector = CassandraConnector.apply (javaSparkContext.getConf)
    val lastRunDateSet: ResultSet = session.execute (String.format ("SELECT id, last_run_date FROM %s %s", cassandraSchemaName, cassandraRfmLastRunTableName))
    if (lastRunDateSet != null) {
      val allDates: util.List[Row] = lastRunDateSet.all()
      val allDatesSize: Integer = allDates.size()

      if (allDatesSize > 0) {

        allDates.sort (new Comparator[Row] {
          override def compare(row1: Row, row2: Row): Int = {
            val row1Id: Integer = row1.getInt (0)
            val row2Id: Integer = row2.getInt (0)

            row1Id.compareTo (row2Id)
          }
        })

        val allDatesLastRow: Row = allDates.get (allDatesSize - 1)
        val lastRunId: Integer = allDatesLastRow.getInt (0)
        val lastRunDate: String = allDatesLastRow.getString (1)
        val currentRunId: Integer = lastRunId + 1

        insertIntoRfmLastRun (session, cassandraSchemaName, cassandraRfmLastRunTableName, currentRunId, currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId)

        for (i <- allDatesSize) {
          val previousRunRow: Row = allDates.get (i)
          val previousRunDate: String = previousRunRow.getString (1)

          if (previousRunDate != null && !previousRunDate.isEmpty) {

            val cassandraLastRunRFMResultsTableName: String = String.format ("rfm_results_%s", lastRunDate)
            val cassandraLastRunSummaryStatisticsTableName: String = String.format ("summary_stats_%s", lastRunDate)
            val cassandraLastRunTop1PercentSummaryStatisticsTableName: String = String.format ("summary_stats_top_1_%s", lastRunDate)
            val cassandraLastRunTop5PercentSummaryStatisticsTableName: String = String.format ("summary_stats_top_5_%s", lastRunDate)
            val cassandraLastRunTop15PercentSummaryStatisticsTableName: String = String.format ("summary_stats_top_15_%s", lastRunDate)
            val cassandraLastRunTop25PercentSummaryStatisticsTableName: String = String.format ("summary_stats_top_25_%s", lastRunDate)
            val cassandraLastRunTop50PercentSummaryStatisticsTableName: String = String.format ("summary_stats_top_50_%s", lastRunDate)
            val cassandraLastRunBottom50PercentSummaryStatisticsTableName: String = String.format ("summary_stats_bottom_50_%s", lastRunDate)

            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunTop1PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunTop5PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunTop15PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunTop25PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunTop50PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunBottom50PercentSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunSummaryStatisticsTableName))
            session.execute (String.format ("DROP TABLE IF EXISTS %s %s", cassandraSchemaName, cassandraLastRunRFMResultsTableName))
          }
        }
      }

        else
        {
          insertFirstJobRunDate (session, cassandraSchemaName, cassandraRfmLastRunTableName, currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId)
        }
      }

      else {
        insertFirstJobRunDate (session, cassandraSchemaName, cassandraRfmLastRunTableName, currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId)
      }
    }
  finally {
    if (session != null && !session.isClosed) {
      session.close()
    }
  }
  }

def insertIntoRfmLastRun(session: Session, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, currentRunId: Integer, currentDateTimePath: String, lastRunCustomerId: BigInteger, lastRunCustomerAddressId: Integer, lastRunOrderId: Integer): Unit = {
  session.execute (String.format ("INSERT INTO %s.%s (id, last_run_date, last_run_customer_id, last_run_customer_address_id, last_run_order_id) values (%s, '%s', %s, %s, %s)", cassandraSchemaName, cassandraRfmLastRunTableName, currentRunId, currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId))
}
  def insertFirstJobRunDate(session: Session, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, currentDateTimePath: String, lastRunCustomerId: BigInteger, lastRunCustomerAddressId: Integer, lastRunOrderId: Integer): Unit = {
    insertIntoRfmLastRun (session, cassandraSchemaName, cassandraRfmLastRunTableName, 1, currentDateTimePath, lastRunCustomerId, lastRunCustomerAddressId, lastRunOrderId)
  }

def GetSpeedLayerFilteredCustomers(sparkContext: JavaSparkContext, session: Session, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, customersTableName: String): JavaRDD[Customer] = {
  var filteredCustomersJavaRDD: JavaRDD[Customer] = null
  val lastRunCustomerId: BigInteger = GetLastRunCustomerId (sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
  val customers: util.List[Customer]= getSpeedLayerCustomers (session, cassandraSchemaName, customersTableName)
  val customersJavaRDD: JavaRDD[Customer] = sparkContext.parallelize (customers)
  // Filter customers based on the last run customer id from the nightly batch job
  if (lastRunCustomerId != null) {
    var filteredCustomersJavaRDD: JavaRDD[Customer] = getFilteredCustomers (customersJavaRDD, lastRunCustomerId, true)
  }
  else {
    filteredCustomersJavaRDD = customersJavaRDD
  }
   filteredCustomersJavaRDD
}

  def GetSpeedLayerFilteredCustomerAddresses(sparkContext: JavaSparkContext, session: Session, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, customerAddressesTableName: String): JavaRDD[CustomerAddress] = {

    var filteredCustomerAddressJavaRDD: JavaRDD[CustomerAddress] = null
    var lastRunCustomerAddressId: Integer = GetLastRunCustomerAddressId (sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
    var customerAddresses: util.List[CustomerAddress] = getSpeedLayerCustomerAddresses (session, cassandraSchemaName, customerAddressesTableName)
    var customerAddressJavaRDD: JavaRDD[CustomerAddress] = sparkContext.parallelize (customerAddresses)
    // Filter customer addresses based on the last run customer id from the nightly batch job
    if (lastRunCustomerAddressId != null) {
      filteredCustomerAddressJavaRDD = getFilteredCustomerAddresses (customerAddressJavaRDD, lastRunCustomerAddressId, true)
    }
    else {
      filteredCustomerAddressJavaRDD = customerAddressJavaRDD
    }

    filteredCustomerAddressJavaRDD
  }

  def GetSpeedLayerFilteredOrders(sparkContext: JavaSparkContext, session: Session, cassandraSchemaName: String, cassandraRfmLastRunTableName: String, ordersTableName: String): JavaRDD[Order] = {
    var filteredOrderJavaRDD: JavaRDD[Order] = null
    var orders: util.List[Order] = getSpeedLayerOrders (session, cassandraSchemaName, ordersTableName)
    var lastRunOrderId: Integer =  GetLastRunOrderId (sparkContext, cassandraSchemaName, cassandraRfmLastRunTableName)
    var orderJavaRDD: JavaRDD[Order] = sparkContext.parallelize (orders)


    // Filter orders based on the last run customer id from the nightly batch job
    if (lastRunOrderId != null) {
      filteredOrderJavaRDD = getFilteredOrders (orderJavaRDD, lastRunOrderId, true)
    }
    else {
      filteredOrderJavaRDD = orderJavaRDD
    }

    filteredOrderJavaRDD
  }

  def GetSpeedLayerSummaryStatsApplicationIds(session: Session, cassandraSchemaName: String, cassandraSummaryStatsTableName: String): util.List[Integer] = {
    var summaryStatsResultSet: ResultSet = session.execute (String.format ("SELECT * FROM %s %s", cassandraSchemaName, cassandraSummaryStatsTableName))
    var applicationIds: util.List[Integer] = new util.ArrayList[Integer]()

    if (summaryStatsResultSet != null) {

      for (row <- summaryStatsResultSet.all) {
        val applicationId: Integer = row.get (0, classOf [Integer])
        applicationIds.add (applicationId)
      }
    }

    applicationIds
  }


  def getFilteredOrders(orderJavaRDD: JavaRDD[Order], lastRunOrderId: Integer, greaterThan: Boolean): JavaRDD[Order] = {
     orderJavaRDD.filter(new Function[Order, Boolean]() {
      @throws[Exception]
      def call(order: Order): Boolean = {
        if (greaterThan) {
           order.getId > lastRunOrderId
        }
        else {
           order.getId <= lastRunOrderId
        }
      }

       override def apply(v1: Order): Boolean = null
     })
  }

 def getFilteredCustomerAddresses(customerAddressJavaRDD: JavaRDD[CustomerAddress], lastRunCustomerAddressId: Integer, greaterThan: Boolean): JavaRDD[CustomerAddress] = {
   customerAddressJavaRDD.filter(new Function[CustomerAddress, Boolean]() {
     @throws[Exception]
     def call(customerAddress: CustomerAddress): Boolean = {
       if (greaterThan) {
         customerAddress.getID > lastRunCustomerAddressId
       }
       else {
         customerAddress.getID <= lastRunCustomerAddressId
       }
     }

     override def apply(v1: CustomerAddress): Boolean = null
   })
   }

  def getFilteredCustomers(customersJavaRDD: JavaRDD[Customer], lastRunCustomerId: BigInteger, greaterThan: Boolean): JavaRDD[Customer] = {
    customersJavaRDD.filter(new Function[Customer, Boolean]() {
      @throws[Exception]
      def call(customer: Customer): Boolean = {
        if (greaterThan) {
          customer.getId.compareTo (lastRunCustomerId) > 0
        }
        else {
          customer.getId.compareTo (lastRunCustomerId) <= 0
        }
      }

      override def apply(v1: Customer): Boolean = null
    })
  }

 }





