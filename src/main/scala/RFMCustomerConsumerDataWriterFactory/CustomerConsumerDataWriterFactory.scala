package RFMCustomerConsumerDataWriterFactory

/**
  * Created by kartheekvadlamani on 9/17/16.
  */

import RFMCustomer.Customer
import RFMCustomerDataWriter.CustomerDataWriter
import com.datastax.spark.connector.ColumnRef
import com.datastax.spark.connector.cql.TableDef
import com.datastax.spark.connector.writer.{RowWriter, RowWriterFactory}

class CustomerConsumerDataWriterFactory extends RowWriterFactory[Customer] {
  override def rowWriter(table: TableDef, selectedColumns: scala.IndexedSeq[ColumnRef]): RowWriter[Customer] = new CustomerDataWriter
}

